"""
API Models' serializers
"""
from rest_framework import serializers

from cinema.models import Movie, Hall, User, Schedule, Ticket


class MovieSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ['movie_id', 'movie_title', 'movie_length']


class HallSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hall
        fields = ['hall_id', 'hall_name', 'hall_rows_number', 'hall_places_number']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'is_active', 'is_admin', 'password']
        extra_kwargs = {'password': {'write_only': True}}


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ['schedule_id', 'movie_id', 'hall_id', 'start_time', 'price']


class TicketSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ticket
        fields = ['ticket_id', 'user_id', 'row', 'place', 'booking_time', 'payment_time', 'price', 'hall', 'schedule']
