from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from cinema.views import UserRegister, UserRetrieveUpdateAPIView, HallView, MovieView, ScheduleView, TicketView, \
    authenticate_user

urlpatterns = [
    path('user_register/', UserRegister.as_view()),
    path('get_token/', authenticate_user),
    path('user_info/<int:pk>/', UserRetrieveUpdateAPIView.as_view()),
    path('hall/<str:hall_name>/', HallView.as_view()),
    path('movie/', MovieView.as_view()),
    path('movie/<str:movie_id>/', MovieView.as_view()),
    path('schedule/', ScheduleView.as_view()),
    path('schedule/<int:pk>/', ScheduleView.as_view()),
    path('ticket/<int:pk>/', TicketView.as_view()),
    path('ticket/', TicketView.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)
