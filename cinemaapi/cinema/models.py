
# Create your models here.
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models, transaction
from django.db.models import CASCADE, Model
from django.db.transaction import TransactionManagementError


class Movie(Model):
    """Model for a movie in the cinema"""
    movie_id = models.fields.AutoField(name='movie_id', primary_key=True)
    movie_title = models.fields.CharField(name='movie_title', null=False, blank=False, max_length=500)
    movie_length = models.fields.PositiveIntegerField(name='movie_length', null=False)


class Hall(Model):
    """Model for a cinema hall"""
    hall_id = models.fields.AutoField(name='hall_id', primary_key=True)
    hall_name = models.fields.CharField(name='hall_name', unique=True, max_length=200)
    hall_rows_number = models.fields.PositiveSmallIntegerField(name='hall_rows_number')
    hall_places_number = models.fields.PositiveSmallIntegerField(name='hall_places_number')


class UserManager(BaseUserManager):

    def _create_user(self, email, password, **extra_fields):
        if not email:
            raise ValueError('The email must be set')
        try:
            with transaction.atomic():
                user = self.model(email=email, **extra_fields)
                user.set_password(password)
                user.save(using=self._db)
                return user
        except TransactionManagementError as e:
            raise e

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_admin', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_admin', True)
        extra_fields.setdefault('is_superuser', True)

        return self._create_user(email, password=password, **extra_fields)


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    """
    email = models.EmailField(max_length=40, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    # REQUIRED_FIELDS = ['email']

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        return self


class Schedule(Model):
    schedule_id = models.fields.AutoField(name='schedule_id', primary_key=True)
    movie_id = models.ForeignKey(to=Movie, on_delete=CASCADE)
    hall_id = models.ForeignKey(to=Hall, on_delete=CASCADE)
    start_time = models.fields.DateTimeField(name='start_time')
    price = models.fields.FloatField(name='price')


class Ticket(Model):
    ticket_id = models.fields.AutoField(name='ticket_id', primary_key=True)
    user_id = models.ForeignKey(to=User, on_delete=CASCADE)
    hall = models.ForeignKey(to=Hall, on_delete=CASCADE)
    schedule = models.ForeignKey(to=Schedule, on_delete=CASCADE)
    row = models.fields.SmallIntegerField(name='row')
    place = models.fields.SmallIntegerField(name='place')
    booking_time = models.fields.DateTimeField(name='booking_time', blank=True, null=True)
    payment_time = models.fields.DateTimeField(name='payment_time', blank=True, null=True)
    price = models.fields.FloatField(name='price', default=0, null=False, blank=False)
